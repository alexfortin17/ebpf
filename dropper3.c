#include <linux/kernel.h>
#include <linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/icmp.h>
#include <linux/stddef.h>
#include "bpf_helpers.h"
#include "xdp_drop_stat.h"

//clang -O2 -target bpf -I/usr/include/x86_64-linux-gnu/ -c x.c -o x.o
//sudo ip link set dev ethX xdp obj x.o sec dropper_main

/* counter map */
//struct bpf_map_def SEC("maps") blacklist = {
//	.type = BPF_MAP_TYPE_PERCPU_HASH,
//	.key_size = sizeof(__u32),
//	.value_size = sizeof(__u64),
//	.max_entries = 100000,
//	.map_flags = 0
//};

#define SEC(NAME) __attribute__((section(NAME), used))

#define trace_printk(fmt, ...) do { \
	char _fmt[] = fmt; \
	bpf_trace_printk(_fmt, sizeof(_fmt), ##__VA_ARGS__); \
        } while (0)



static inline int parse_ipv4(void *data, __u64 nh_off, void *data_end, __be32 *src, __be32 *dest)
{
	struct iphdr *iph = data + nh_off;
	// check to satisfy verifier
	if (iph + 1 > data_end)
	{
		return 0;
	}
        *src = iph->saddr;
	*dest = iph->daddr;
	return iph->protocol;
}

SEC("dropper_main")
int dropper(struct xdp_md *ctx)
{
	int ipsize = 0;
	long *value;
	void *data = (void *)(long)ctx->data;
	void *data_end = (void *)(long)ctx->data_end;

	struct ethhdr *eth = data;
	struct stats_entry *stats;
	__be32 dest_ip, src_ip;
	__u16 h_proto;
	__u64 nh_off;
	int ipproto;

	nh_off = sizeof(*eth);
	struct iphdr *iph = data + nh_off;


	//Check for verifier
	if (data + nh_off > data_end)
	{
		return XDP_PASS;
	}

	ipproto = parse_ipv4(data, nh_off, data_end, &src_ip, &dest_ip);
//	if ( __constant_ntohl(dest_ip) == 0xac110001 )
//        {
//		//int devil = 666;
//		//trace_printk("%d\n", devil);
//                trace_printk("xdp_drop\n");
//		return XDP_DROP;
//	}
	if (ipproto == IPPROTO_ICMP){
		trace_printk("xdp_drop\n");
		value = bpf_map_lookup_elem(&blacklist, &ipproto);
		if(value){
			*value += 1;
		}
                return XDP_DROP;
	}

	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
