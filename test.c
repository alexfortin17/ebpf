#include <linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/icmp.h>

#define SEC(NAME) __attribute__((section(NAME), used))

/* To compile: clang -O2 -target bpf -I/usr/include/x86_64-linux-gnu/ -c x.c -o x.o */
/* To insert: sudo ip link set dev ethX xdp obj x.o sec dropper_main */

static inline int parse_ipv4(void *data, __u64 nh_off, void *data_end,
			     __be32 *src, __be32 *dest)
{
	struct iphdr *iph = data + nh_off;
	//Necessary check for the parser not to whine about potential overflows
	if (iph + 1 > data_end)
		return 0;

	*src = iph->saddr;
	*dest = iph->daddr;
	return iph->protocol;
}

SEC("dropper_main")
int dropper(struct xdp_md *ctx) {
  int ipsize = 0;

  void *data = (void *)(long)ctx->data;
  void *data_end = (void *)(long)ctx->data_end;

  struct ethhdr *eth = data;
  __be32 dest_ip, src_ip;
  __u16 h_proto;
  __u64 nh_off;
  int ipproto;

  nh_off = sizeof(*eth);
  //Necessary check for the parser not to whine about potential overflows
  if (data + nh_off > data_end)
	  return XDP_PASS;


  h_proto = eth->h_proto;
  ipproto = parse_ipv4(data, nh_off, data_end, &src_ip, &dest_ip);

  // Dropping all ICMP traffic
  if (ipproto == IPPROTO_ICMP)
    return XDP_DROP;

  // example checking eth header
  //if (h_proto == __constant_htons(ETH_P_IP))
  //	return XDP_DROP;
  

  return XDP_PASS;
}
